# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin

from .models import User

class UserAdmin(DjangoUserAdmin):
    add_form_template = None
    fieldsets = None
    ordering = ('email',)
    list_filter = ('is_superuser', 'is_active', 'groups')
    list_display = ('email', 'first_name', 'last_name', 'is_superuser')
    search_fields = ('email', 'first_name', 'last_name')


admin.site.register(User, UserAdmin)
