/**
 * Clear whitespaces
 * @param node 
 */

function cleanNode(node) {
    for (var n = 0; n < node.childNodes.length; n++) {
        var child = node.childNodes[n];

        if (child.nodeType === 8 || (child.nodeType === 3 && !/\S/.test(child.nodeValue))) {
            node.removeChild(child);
            n--;
        } else if (child.nodeType === 1) {
            cleanNode(child);
        }
    }
};

/**
 * Header
 * Dropdowns
 */

$(document).keyup(function (e) {
    if (e.key === "Escape") {
        closeStoreDropdown();
        closeProfileDropdown();
    }
});

$(window).click(function () {
    closeStoreDropdown();
    closeProfileDropdown();
});

$('#storeDropdownBtn').click(function (event) {
    event.stopPropagation();
    $('#storeDropdown').toggle(50);
    $('#profileDropdown').hide(50);
});

$('#profileDropdownBtn').click(function (event) {
    event.stopPropagation();
    $('#profileDropdown').toggle(50);
    $('#storeDropdown').hide(50);
});

$('#storeDropdownBtnSM').click(function (event) {
    event.stopPropagation();
    $('#storeDropdown').toggle(50);
    $('#profileDropdown').hide(50);
});

$('#profileDropdownBtnSM').click(function (event) {
    event.stopPropagation();
    $('#profileDropdown').toggle(50);
    $('#storeDropdown').hide(50);
});

function closeStoreDropdown() {
    $('#storeDropdown').hide(50);
};

function closeProfileDropdown() {
    $('#profileDropdown').hide(50);
};

/**
 * Sticky navigation
 */

// When the user scrolls the page, execute stickyNav
window.onscroll = function () {
    stickyNav()
};

// Get the navbar
var navbar;

// Get the offset position of the navbar
var sticky;

if (document.getElementById('navbar')) {
    // Get the navbar
    navbar = document.getElementById('navbar');

    // Get the offset position of the navbar
    sticky = navbar.offsetTop;
}

// Add the sticky class to the navbar when you reach its scroll position. Remove 'sticky' when you leave the scroll position
function stickyNav() {
    if (window.pageYOffset >= sticky) {
        navbar.classList.add('sticky-nav')
    } else {
        navbar.classList.remove('sticky-nav');
    }
}

/**
 * Forms
 */

function togglePassword(id) {

    var input = $(id)[0];

    if (input.type === 'password') {
        input.type = 'text';
    } else {
        input.type = 'password';
    }
};

/* image input onchange */
$("#id_image").change(function () {

    var input = this;
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageInput').css(
                'background-image', 'url(' + e.target.result + ')'
            );
        };

        reader.readAsDataURL(input.files[0]);
    }
});

/**
 * Views
 */

function dashboardResize() {
    if ($('#dashboardWidth')) {
        var referenceW = $('#dashboardWidth').innerWidth()
        var changebleW = $('#dashboardTable');

        if ($(window).width() <= '1124') {
            changebleW.width(referenceW - 32);
        } else {
            changebleW.width('unset');
        }
    }
};

dashboardResize();

$(window).resize(function () {
    dashboardResize();
});

/**
 * Orders
 */
$('#orderModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);

    var number = button.data('number');
    var date = button.data('date');
    var customer = button.data('customer');
    var email = button.data('email');
    var phone = button.data('phone');
    var picked = button.data('picked');
    var delivered = button.data('delivered');
    
    var modal = $(this);
    
    modal.find('.m-order-number').text(number);
    modal.find('.m-order-date').text(date);
    modal.find('.m-order-customer').text(customer);
    modal.find('.m-order-email').text(email);
    modal.find('.m-order-phone').text(phone);
    modal.find('.m-order-picked').text(picked);
    modal.find('.m-order-delivered').text(delivered);

    // modal.find('.modal-body input').val(recipient);
});

/**
 * Items
 */
$('#itemModal').on('show.bs.modal', function (event) {
    
    var button = $(event.relatedTarget);
    var name = button.data('name');
    var id = button.data('id');   
    var modal = $(this);
    
    modal.find('.m-item-name').text(name);
    modal.find('.m-confirm-delete').attr('action', '/item/delete/' + id + '/');
});

/**
 * Tooltip
 */
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});