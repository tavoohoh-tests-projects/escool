from django.conf.urls import url

from school.views import (HomeRedirect, Login, Logout, StudentClassrooms,
                          StudentHome, TeacherHome, TeacherLessonDetail,
                          TeacherLessons)

urlpatterns = [
    url(r'^login/$', Login.as_view(), name='login'),
    url(r'^logout/$', Logout.as_view(), name='logout'),
    url(r'^teacher/home/$', TeacherHome.as_view(), name='teacher_home'),
    url(
        r'^teacher/subjects/edit/$',
        TeacherLessons.as_view(),
        name='teacher_lessons'
    ),
    url(
        r'^teacher/subjects/(?P<pk>[0-9A-Za-z]+)/$',
        TeacherLessonDetail.as_view(),
        name='teacher_lesson_detail'
    ),
    url(r'^student/home/$', StudentHome.as_view(), name='student_home'),
    url(
        r'^student/classrooms/$',
        StudentClassrooms.as_view(),
        name='student_classrooms'
    ),
    url(r'', HomeRedirect.as_view(), name='redirect_to')
]
