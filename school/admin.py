# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Lesson, ClassRoom, Teacher, Student


admin.site.register(Lesson)
admin.site.register(ClassRoom)
admin.site.register(Teacher)
admin.site.register(Student)
