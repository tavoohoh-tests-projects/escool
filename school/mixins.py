from django.urls import reverse_lazy


class GlobalMixins():

    def redirectTo(self, request):
        if request.user.is_authenticated():
            if request.user.is_admin:
                url = '/admin/'
            elif request.user.is_teacher:
                url = reverse_lazy('teacher_home')
            elif request.user.is_student:
                url = reverse_lazy('student_home')
        else:
            url = reverse_lazy('login')
        
        return url