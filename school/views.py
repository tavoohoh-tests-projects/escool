# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import logout as auth_logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import (CreateView, ListView, RedirectView,
                                  TemplateView, UpdateView, DetailView)

from school.forms import LoginForm, ChangeLessonForm, ChangeClassRoomForm
from school.mixins import GlobalMixins
from school.models import Student, Teacher, ClassRoom, Lesson


class Login(LoginView, GlobalMixins):
    '''
    Provides the ability to login
    '''
    success_url = reverse_lazy('redirect_to')
    template_name = 'login.html'
    form_class = LoginForm


class Logout(LoginRequiredMixin, RedirectView):
    """
    Provides users the ability to logout
    """
    url = reverse_lazy('login')

    def get(self, request, *args, **kwargs):
        auth_logout(request)
        response = super(Logout, self).get(request, *args, **kwargs)
        return response


class HomeRedirect(RedirectView, GlobalMixins):
    def get_redirect_url(self, *args, **kwargs):
        print('-----------------')
        print(self.request)
        return self.redirectTo(self.request)


class TeacherHome(ListView):
    template_name = 'teacher/home.html'
    context_object_name = 'teacher'
    model = Teacher

    def get_context_data(self, **kwargs):

        ctx = super(TeacherHome, self).get_context_data(**kwargs)

        user = self.request.user.id
        teacher = get_object_or_404(Teacher, user=user)

        ctx.update({
            'teacher': teacher,
            'lessons': ClassRoom.objects.filter(
                teacher=teacher
            ),
        })

        return ctx


class TeacherLessons(UpdateView):
    form_class = ChangeLessonForm
    success_url = reverse_lazy('teacher_home')
    model = Teacher
    template_name = 'teacher/form.html'

    def get_object(self):
        user = self.request.user.id
        teacher = get_object_or_404(Teacher, user=user)
        return teacher

    def form_valid(self, form):
        self.object = form.save(commit=False)

        teacher = get_object_or_404(Teacher, user=self.request.user)
        classrooms = ClassRoom.objects.filter(teacher=teacher)
        lessons = form.cleaned_data['lessons']

        for classroom in classrooms:
            classroom.delete()
            

        for lesson in lessons:
            ClassRoom.objects.create(
                lesson=lesson,
                teacher=teacher
            ).save()

        self.object.save()

        return super(TeacherLessons, self).form_valid(form)


class TeacherLessonDetail(DetailView):
    model = ClassRoom
    template_name = 'teacher/lesson_detail.html'

    def get_context_data(self, **kwargs):

        ctx = super(TeacherLessonDetail, self).get_context_data(**kwargs)

        classroom = get_object_or_404(
            ClassRoom,
            id=self.kwargs.get('pk')
        )

        ctx.update({
            'lesson': classroom,
            'students': Student.objects.filter(
                classroom=classroom
            )
        })

        return ctx


class StudentHome(ListView):
    template_name = 'student/home.html'
    context_object_name = 'student'
    model = Student

    def get_queryset(self):
        user = self.request.user.id
        student = get_object_or_404(Student, user=user)
        return student


class StudentClassrooms(UpdateView):
    template_name = 'student/form.html'
    form_class = ChangeClassRoomForm
    success_url = reverse_lazy('student_home')
    model = Student

    def get_object(self):
        user = self.request.user.id
        student = get_object_or_404(Student, user=user)
        return student

    def get_context_data(self, **kwargs):

        ctx = super(StudentClassrooms, self).get_context_data(**kwargs)

        ctx.update({
            'lessons': Lesson.objects.all()
        })

        return ctx