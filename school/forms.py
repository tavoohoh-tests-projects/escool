from django import forms
from django.contrib.auth.forms import AuthenticationForm, UsernameField
from django.utils.translation import ugettext_lazy as _

from school.models import ClassRoom, Lesson, Student, Teacher


class LoginForm(AuthenticationForm):
    username = UsernameField(
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True}),
    )
    password = forms.CharField(
        strip=False,
        widget=forms.PasswordInput,
    )


class ChangeLessonForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = [
            'lessons',
        ]


class CustomModelChoiceIterator(forms.models.ModelChoiceIterator):
    def choice(self, obj):
        return (self.field.prepare_value(obj),
                self.field.label_from_instance(obj), obj)


class CustomModelChoiceField(forms.models.ModelMultipleChoiceField):
    def _get_choices(self):
        if hasattr(self, '_choices'):
            return self._choices
        return CustomModelChoiceIterator(self)

    choices = property(_get_choices,
                       forms.MultipleChoiceField._set_choices)


class ChangeClassRoomForm(forms.ModelForm):
    # classroom = CustomModelChoiceField(
    #     widget=forms.CheckboxSelectMultiple,
    #     queryset=ClassRoom.objects.all()
    # )

    class Meta:
        model = Student
        fields = [
            'classroom'
        ]
