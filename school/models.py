# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import gettext_lazy as _

from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save


class Lesson(models.Model):
    title = models.CharField(
        verbose_name=_('Lesson title'),
        max_length=255,
    )
    description = models.TextField(
        verbose_name=_('Description'),
        null=True,
        blank=True
    )

    class Meta:
        verbose_name=_('Lesson')
        verbose_name_plural=_('Lessons')
        permissions=(
            ('manage_lessons', 'Can add, change and delete lessons'),
            ('view_lessons', 'Can view lessons'),
        )

    def __str__(self):
        return self.title


class Teacher(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='teacher_user'
    )

    lessons = models.ManyToManyField(
        Lesson,
        related_name='teacher_lesson',
        blank=True
    )

    class Meta:
        verbose_name = _('Teacher')
        verbose_name_plural = _('Teachers')
        permissions = (
            ("manage_teachers", "Can add, change and delete teachers"),
            ("view_teachers", "Can view teachers"),
        )

    def __str__(self):
        return self.user.get_full_name()


class ClassRoom(models.Model):
    lesson = models.ForeignKey(
        Lesson,
        on_delete=models.DO_NOTHING,
        related_name='classroom_lesson',
        null=True,
        blank=True
    )

    teacher = models.ForeignKey(
        Teacher,
        on_delete=models.DO_NOTHING,
        related_name='classroom_teacher',
        null=True,
        blank=True
    )

    class Meta:
        verbose_name=_('Classroom')
        verbose_name_plural=_('Classrooms')
        permissions=(
            ('manage_classrooms', 'Can add, change and delete classrooms'),
            ('view_classrooms', 'Can view classrooms'),
        )

    def __str__(self):
        lesson_str = '%s with teacher %s' % (self.lesson.title, self.teacher)
        return lesson_str.strip()


class Student(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name='student_user'
    )

    classroom = models.ManyToManyField(
        ClassRoom,
        related_name='student_lessons',
        blank=True
    )

    class Meta:
        verbose_name = _('Student')
        verbose_name_plural = _('Students')
        permissions = (
            ("manage_students", "Can add, change and delete students"),
            ("view_students", "Can view students"),
        )

    def __str__(self):
        return self.user.get_full_name()
